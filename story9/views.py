from story9.forms import CreateUserForm
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import messages
from .models import *
from django.contrib.auth import authenticate, login as dj_login, logout

# Create your views here.
def login (request):
    if request.user.is_authenticated:
        return redirect ('/landing')
    else:
        if (request.method == 'POST'):
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)

            if user is not None:
                dj_login(request, user)
                return redirect('/landing')
            else:
                messages.error(request, 'Username atau Password anda salah ')
        
        context={}
        return render (request, 'login.html', context)

def landing(request):
    return render (request, 'landing.html')

def regis(request):
    if request.user.is_authenticated:
	    return redirect('/landing')
    else:
        form = CreateUserForm()
        if (request.method == 'POST'):
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, 'Akun sudah dibuat')
                return redirect('/landing/login')
        
        context = {'form': form}
        return render (request,'regis.html', context)

def logoutUser(request):
    logout(request)
    return redirect ('/landing')