from django.test import TestCase, Client
from django.urls import resolve
from .views import page
from .apps import Home7Config
from django.apps import apps

class UnitTestStory7(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_function_story7(self):
        found = resolve('/')
        self.assertEqual(found.func, page)
    def test_index_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'page.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Home7Config.name, 'home7')
        self.assertEqual(apps.get_app_config('home7').name, 'home7')
