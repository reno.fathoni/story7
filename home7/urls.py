from django.urls import path

from . import views

app_name = 'home7'

urlpatterns = [
    path('', views.page, name='page'),
]