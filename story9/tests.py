# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import login, landing, regis, logoutUser
from .apps import Story9Config
from django.apps import apps

class UnitTestStor9(TestCase):
    def test_landing_url_is_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code, 200)

    def test_function_landing(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, landing)

    def test_landing_is_using_correct_html(self):
        response = Client().get('/landing/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_login_url_is_exist(self):
        response = Client().get('/landing/login/')
        self.assertEqual(response.status_code, 200)

    def test_function_login(self):
        found = resolve('/landing/login/')
        self.assertEqual(found.func, login)

    def test_login_is_using_correct_html(self):
        response = Client().get('/landing/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_regis_url_is_exist(self):
        response = Client().get('/landing/signup/')
        self.assertEqual(response.status_code, 200)
        
    def test_function_regis(self):
        found = resolve('/landing/signup/')
        self.assertEqual(found.func, regis)

    def test_regis_is_using_correct_html(self):
        response = Client().get('/landing/signup/')
        self.assertTemplateUsed(response, 'regis.html')

    def test_logout_url_is_exist(self):
        response = Client().get('/landing/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_function_logout(self):
        found = resolve('/landing/logout/')
        self.assertEqual(found.func, logoutUser)

    

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')
