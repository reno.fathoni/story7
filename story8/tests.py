from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import ind
from .apps import Story8Config
from django.apps import apps

class UnitTestStory8(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    def test_function_story8(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, ind)
    def test_ind_is_using_correct_html(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'ind.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')