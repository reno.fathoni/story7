from django.shortcuts import render
from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponse
import json
import requests

# Create your views here.

def ind(request):
    response = {}
    return render(request, 'ind.html',response)

def fungsi_data(request):
    arg  = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    req = requests.get(url_tujuan)
    data = json.loads(req.content)
    return JsonResponse(data, safe=False)

