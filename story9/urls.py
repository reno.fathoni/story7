from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('login/', views.login, name='login'),
    path('signup/', views.regis, name='regis'),
    path('logout/', views.logoutUser, name='logout'),
]